﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour
{
    // Config
    [SerializeField] float runSpeed = 5f;
    [SerializeField] float jumpSpeed = 5f;
    [SerializeField] float climbSpeed = 5f;
    [SerializeField] Vector2 deathKickLeft = new Vector2(5f, 15f);
    [SerializeField] Vector2 deathKickRigth = new Vector2(-5f, 15f);

    // State
    bool isAlive = true;

    // Cached component references 
    Rigidbody2D myRigidBody;
    Animator myAnimator;
    CapsuleCollider2D myBodyCollider2D;
    BoxCollider2D myFeetCollider2D;

    void Start()
    {
        myRigidBody = GetComponent<Rigidbody2D>();
        myAnimator = GetComponent<Animator>();
        myBodyCollider2D = GetComponent<CapsuleCollider2D>();
        myFeetCollider2D = GetComponent<BoxCollider2D>();
    }

    void Update()
    {
        if (!isAlive) { return; }

        Run();
        Jump();
        ClimbLadder();
        FlipSprite();
        Die();
    }

    private void Die()
    {
        if (myBodyCollider2D.IsTouchingLayers(LayerMask.GetMask("Enemy", "Hazards")))
        {
            isAlive = false;
            myAnimator.SetTrigger("Dying");
            myRigidBody.sharedMaterial.bounciness = 1f;

            if (transform.localScale.x > 0)
                myRigidBody.velocity = deathKickRigth;
            else
                myRigidBody.velocity = deathKickLeft;
        }
        myRigidBody.sharedMaterial.bounciness = 0f;
	}

    private void Run()
    {
        float controlThrow = Input.GetAxis("Horizontal"); // Values between -1 to 1
        Vector2 playerVelocity = new Vector2(controlThrow * runSpeed, myRigidBody.velocity.y);
        myRigidBody.velocity = playerVelocity;

        bool playerHasHorizontalSpeed = Mathf.Abs(myRigidBody.velocity.x) > Mathf.Epsilon;
        myAnimator.SetBool("Running", playerHasHorizontalSpeed);
    }

    private void Jump()
    {
        if (!myFeetCollider2D.IsTouchingLayers(LayerMask.GetMask("Ground"))) {

			return;
		}
           
        if (Input.GetButtonDown("Jump"))
        {
            Vector2 jumpVelocityToAdd = new Vector2(0f, jumpSpeed);
            myRigidBody.velocity += jumpVelocityToAdd;
        }
        
    }

	private IEnumerator JumpCoroutine()
	{
		if (!myFeetCollider2D.IsTouchingLayers(LayerMask.GetMask("Ground", "Ladder")))
		{
			yield return null;
		}

		if (Input.GetButtonDown("Jump"))
		{
			Physics2D.IgnoreLayerCollision(9, 10); // Disable the Collision Layer between the player and ladder.
			myRigidBody.gravityScale = 1;

			Vector2 jumpVelocityToAdd = new Vector2(0f, jumpSpeed);
			myRigidBody.velocity += jumpVelocityToAdd;

			yield return new WaitForSeconds(0.5f);

			Physics2D.IgnoreLayerCollision(9, 10, false); // Enable the Collision Layer between the player and ladder.
		}

	}

	private void ClimbLadder()
    {   
        if (!myBodyCollider2D.IsTouchingLayers(LayerMask.GetMask("Ladder")))
        {
            myAnimator.SetBool("Climbing", false);
            myRigidBody.gravityScale = 1;
			
            return;
        }

        float controlThrow = Input.GetAxis("Vertical"); // Values between -1 to 1
        Vector2 climbVelocity = new Vector2 (0, controlThrow * climbSpeed);
        myRigidBody.velocity = climbVelocity;
        myRigidBody.gravityScale = 0;

		bool playerHasVerticalSpeed = Mathf.Abs(myRigidBody.velocity.y) > Mathf.Epsilon;
        myAnimator.SetBool("Climbing", playerHasVerticalSpeed);
		myAnimator.SetBool("Running", false);
		// Flipar o sprite conforme a direção que o personagem está olhando. 
		
		StartCoroutine(JumpCoroutine());
    }

	private void GetAxisHorizontalDown()
	{
		bool axisHorizontalDown = false;

		if (Input.GetAxisRaw("Horizontal") != 0)
		{
			if (!axisHorizontalDown)
			{
				// do something
				axisHorizontalDown = true;
			}
		}

		if (Input.GetAxisRaw("Horizontal") == 0)
		{
			axisHorizontalDown = false;
		}
	}

    private void FlipSprite()
    {
        bool playerHasHorizontalSpeed = Mathf.Abs(myRigidBody.velocity.x) > Mathf.Epsilon; // Maior do que zero
        if (playerHasHorizontalSpeed)
        {
            transform.localScale = new Vector2 (Mathf.Sign(myRigidBody.velocity.x), transform.localScale.y);
        }
    }
}
